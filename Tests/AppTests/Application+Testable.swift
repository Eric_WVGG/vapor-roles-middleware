//
//  Application+Testable.swift
//  App
//
//  Created by Eric Jacobsen on 8/26/18.
//

import Vapor
@testable import App
import Authentication

extension Application {
    static func testable(envArgs: [String]? = nil) throws -> Application {
        var config = Config.default()
        var env = Environment.testing
        var services = Services.default()
        if let environmentArgs = envArgs {
            env.arguments = environmentArgs
        }
        try App.configure(&config, &env, &services)
        let app = try Application(config: config, environment: env, services: services)
        try App.boot(app)
        return app
    }
    
    static func reset() throws {
        let revertEnvironment = ["vapor", "revert", "--all", "-y"]
        try Application.testable(envArgs: revertEnvironment).asyncRun().wait()
        let migrateEnvironment = ["vapor", "migrate", "-y"]
        try Application.testable(envArgs: migrateEnvironment).asyncRun().wait()
    }
    
    func sendRequest<T>(
        to path: String,
        method: HTTPMethod,
        headers: HTTPHeaders = .init(),
        body: T? = nil,
        loggedInUser: User? = nil
        ) throws -> Response where T: Content {
        var tokenHeaders = HTTPHeaders()
        if( loggedInUser != nil) {
            let credentials = BasicAuthorization(username: loggedInUser!.name, password: "ICU81MI")
            tokenHeaders.basicAuthorization = credentials
        }
        let responder = try self.make(Responder.self)
        let request = HTTPRequest(method: .GET, url: URL(string: path)!, headers: tokenHeaders)
        let wrappedRequest = Request(http: request, using: self)
        if let body = body {
            try wrappedRequest.content.encode(body)
        }
        return try responder.respond(to: wrappedRequest).wait()
    }
    
    // This is just a forwarder to above ^ with empty content
    func sendRequest(
        to path: String,
        method: HTTPMethod,
        headers: HTTPHeaders = .init(),
        loggedInUser: User? = nil
        ) throws -> Response {
        let emptyContent: EmptyContent? = nil
        return try sendRequest(to: path, method: method, headers: headers, body: emptyContent, loggedInUser: loggedInUser)
    }
    
    // This is just a forwarder to above ^^ with data istead of a body.
    // I'm not clear why it doesn't return anything.
    func sendRequest<T>(
        to path: String,
        method: HTTPMethod,
        headers: HTTPHeaders = .init(),
        data: T,
        loggedInUser: User? = nil
        ) throws where T: Content {
        _ = try self.sendRequest(
            to: path, method: method, headers: headers, body: data, loggedInUser: loggedInUser
        )
    }
    
    func getResponse<C, T>(
        to path: String,
        method: HTTPMethod = .GET,
        headers: HTTPHeaders = .init(),
        data: C? = nil,
        decodeTo type: T.Type,
        loggedInUser: User? = nil
        ) throws -> T where C: Content, T: Decodable {
        let response = try self.sendRequest(to: path, method: method, headers: headers, body: data, loggedInUser: loggedInUser)
        return try response.content.decode(type).wait()
    }
    
    func getResponseWithFile<C, T>(
        to path: String,
        method: HTTPMethod = .GET,
        headers: HTTPHeaders = .init(),
        data: C? = nil,
        decodeTo type: T.Type,
        loggedInUser: User? = nil
        ) throws -> T where C: Content, T: Decodable {
        let response = try self.sendRequest(to: path, method: method, headers: headers, body: data, loggedInUser: loggedInUser)
        return try response.content.decode(type).wait()
    }
    
    func getResponseStatus<C>(
        to path: String,
        method: HTTPMethod = .GET,
        headers: HTTPHeaders = .init(),
        data: C? = nil,
        loggedInUser: User? = nil
        ) throws -> HTTPStatus where C: Content {
        let response = try self.sendRequest(to: path, method: method, headers: headers, body: data, loggedInUser: loggedInUser)
        return response.http.status
    }
    
    func getResponseStatus(
        to path: String,
        method: HTTPMethod = .GET,
        headers: HTTPHeaders = .init(),
        loggedInUser: User? = nil
        ) throws -> HTTPStatus {
        let response = try self.sendRequest(to: path, method: method, headers: headers, loggedInUser: loggedInUser)
        return response.http.status
    }
    
    func getResponse<T>(
        to path: String,
        method: HTTPMethod = .GET,
        headers: HTTPHeaders = .init(),
        decodeTo type: T.Type,
        loggedInUser: User? = nil
        ) throws -> T where T: Decodable {
        let emptyContent: EmptyContent? = nil
        return try self.getResponse(to: path, method: method, headers: headers, data: emptyContent, decodeTo: type, loggedInUser: loggedInUser)
    }
    
    struct EmptyContent: Content {}
    
}
