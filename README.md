# Vapor Roles Middleware

This middleware allows you to assign roles like "admin" or "user" to your User accounts, and control access privileges on a router level. It works in conjunction with the Authentication middleware.

## How to implement…

Add this to your Package.swift file

```swift
.package(url: "https://github.com/vapor-community/vapor-roles-middleware.git", from: "1.0.0")
```

This middleware uses Swift generics to support any user and role class (or struct); for the purposes of this document, we’ll assume they’re called `User` and `Role`.

Add RolesMiddleware.swift to your project.

Create a Role enum with migration. You can define any roles you like. For example…

```swift
enum Role: String, Codable, Content, PostgreSQLMigration {
    static let allCases: [Role] = [.basic, .editor, .admin, daemon]
    case basic, editor, admin, daemon
}
```

Add a Role property to your user model. ex. `var role: Role = .basic`

Extend your User model to conform to RoleAuthorizable; identify the Role model, and the user Role property.

```swift
extension User: RoleAuthorizable {
    typealias RoleType = Role
    static var roleKey: RoleKey = \.role
}
```

In your controller, make RoleMiddleware([Role]) rules, and group them into to any protected paths.

```swift
let adminAndEditorRoles = RoleMiddleware<User, Role>([.editor, .admin])
let editorsAndAdminsOnlyAccess = routes.grouped(basicAuthMiddleware, adminAndEditorRoles)
editorsAndAdminsOnlyAccess("editor", use: getOneHandler)
```

## Feedback

Thanks for taking the time to look. I can be reached on Twitter as Eric_WVGG, on the Vapor Discord, or at http://whiskyvangoghgo.com.
