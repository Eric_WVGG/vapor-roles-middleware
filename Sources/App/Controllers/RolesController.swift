//
//  RolesController.swift
//  App
//
//  Created by Eric Jacobsen on 8/26/18.
//

import Vapor
import Fluent
import FluentPostgreSQL
import Authentication
import Crypto

struct RolesController: RouteCollection {
    
    func boot(router: Router) throws {
        let routes = router.grouped("v1")
        
        let basicAuthMiddleware = User.basicAuthMiddleware(using: BCryptDigest())
        
        let basicAccess = routes.grouped(basicAuthMiddleware, RoleMiddleware<User, Role>([.basic, .editor, .admin]))
        basicAccess.get("basic", use: getOneHandler)

        let editorAccess = routes.grouped(basicAuthMiddleware, RoleMiddleware<User, Role>([.editor, .admin]))
        editorAccess.get("editor", use: getEditorPageHandler)

        let adminAccess = routes.grouped(basicAuthMiddleware, RoleMiddleware<User, Role>([.admin]))
        adminAccess.get("admin", use: getAdminPageHandler)
    }
    
    func loginHandler(_ req: Request) throws -> String {
        let user = try req.requireAuthenticated(User.self)
        return "Hello \(user.name)."
    }
     
    func getOneHandler(_ req: Request) throws -> String {
        return "Welcome User (or Editor or Admin)."
    }
    
    func getEditorPageHandler(_ req: Request) throws -> String {
        return "Welcome, Editor (or Admin)."
    }
    
    func getAdminPageHandler(_ req: Request) throws -> String {
        return "Welcome, Admin."
    }
    

}
