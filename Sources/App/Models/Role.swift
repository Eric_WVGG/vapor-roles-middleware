//
//  UserRole.swift
//  App
//
//  Created by Eric Jacobsen on 8/26/18.
//

import Vapor
import FluentPostgreSQL

enum Role: String, Codable, Content, PostgreSQLEnum, PostgreSQLMigration {
    static let allCases: [Role] = [.basic, .editor, .admin]
    case basic, editor, admin
}

